import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { Row, Column } from 'vue-grid-responsive';
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false;
Vue.component('row', Row);
Vue.component('column', Column);
Vue.use(VueAxios, axios);
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
